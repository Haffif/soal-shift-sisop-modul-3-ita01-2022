#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>

// decode stuff begins

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};

void build_decoding_table()
{

    decoding_table = malloc(256);

    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char)encoding_table[i]] = i;
}

void base64_cleanup()
{
    free(decoding_table);
}

char *base64_encode(const unsigned char *data,
                    size_t input_length,
                    size_t *output_length)
{

    *output_length = 4 * ((input_length + 2) / 3);

    char *encoded_data = malloc(*output_length);
    if (encoded_data == NULL)
        return NULL;

    for (int i = 0, j = 0; i < input_length;)
    {

        uint32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (int i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';

    return encoded_data;
}

unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length)
{

    if (decoding_table == NULL)
        build_decoding_table();

    if (input_length % 4 != 0)
        return NULL;

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=')
        (*output_length)--;
    if (data[input_length - 2] == '=')
        (*output_length)--;

    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL)
        return NULL;

    for (int i = 0, j = 0; i < input_length;)
    {

        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6) + (sextet_b << 2 * 6) + (sextet_c << 1 * 6) + (sextet_d << 0 * 6);

        if (j < *output_length)
            decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length)
            decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length)
            decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}

// decode stuff ends

// fungsi2

void makedir(char *arg[])
{
    int status;
    pid_t child;
    child = fork();
    if (child == 0)
    {
        execv("/bin/mkdir", arg);
    }
    else
    {
        ((wait(&status)) > 0);
    }
}

void *unzip(void *arg)
{
    int *n = (int *)arg;
    int num = *n;
    char *mus[] = {"unzip", "-qo", "music.zip", "-d", "/home/ubuntu/sisop/m3s1/music", NULL};
    char *quo[] = {"unzip", "-qo", "quote.zip", "-d", "/home/ubuntu/sisop/m3s1/quote", NULL};

    int status;
    pid_t child;
    if (num == 1)
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/unzip", mus);
        }
        else
        {
            ((wait(&status)) > 0);
        }
    }
    else
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/unzip", quo);
        }
        else
        {
            ((wait(&status)) > 0);
        }
    }
}

void *decode(void *arg)
{
    int *n = (int *)arg;
    int num = *n;

    if (num == 1)
    {
        // printf("Data from music:");
        FILE *mout = fopen("music.txt", "w");
        for (int i = 1; i < 10; i++)
        {
            char c[100];
            FILE *fptr;
            char path[20] = "./music/m";
            char temp[20];
            sprintf(temp, "%d.txt", i);
            strcat(path, temp);
            // printf("\n%s\n", path);
            if ((fptr = fopen(path, "r")) == NULL)
            {
                printf("Error! File cannot be opened.");
                // Program exits if the file pointer returns NULL.
                exit(1);
            }
            // reads text until newline is encountered
            fscanf(fptr, "%[^\n]", c);
            long decode_size = strlen(c);
            char *plain = base64_decode(c, decode_size, &decode_size);
            fprintf(mout, "%s\n", plain);
            fclose(fptr);
        }
        fclose(mout);
    }
    else
    {
        // printf("Data from quote:");
        FILE *qout = fopen("quote.txt", "w");
        for (int i = 1; i < 10; i++)
        {
            char c[100];
            FILE *fptr;
            char path[20] = "./quote/q";
            char temp[20];
            sprintf(temp, "%d.txt", i);
            strcat(path, temp);
            // printf("\n%s\n", path);
            fptr = fopen(path, "r");
            // reads text until newline is encountered
            fscanf(fptr, "%[^\n]", c);
            long decode_size = strlen(c);
            char *plain = base64_decode(c, decode_size, &decode_size);
            fprintf(qout, "%s\n", plain);
            fclose(fptr);
        }
        fclose(qout);
    }
}

void zip()
{
    int status;
    pid_t child;
    child = fork();
    if (child == 0)
    {
        char *argv[] = {"zip", "-r", "-m", "-q", "hasil.zip", "-P", "mihinomenestfaris", "hasil", NULL};
        execv("/bin/zip", argv);
    }
    else
    {
        ((wait(&status)) > 0);
    }
}

void minggat(char file[])
{
    int status;
    pid_t child;
    child = fork();
    char bash[] = "mv";
    char target[] = "./hasil";
    if (child == 0)
    {
        execlp(bash, bash, file, target, NULL);
    }
    else
    {
        ((wait(&status)) > 0);
    }
}

void * final(void *arg)
{
    int *n = (int *)arg;
    int num = *n;

    char *hasil[] = {"unzip", "-P", "mihinomenestfaris", "-qo", "hasil.zip", "-d", "/home/ubuntu/sisop/m3s1/hasil", NULL};
    int status;
    pid_t child;
    if (num == 1)
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/unzip", hasil);
        }
        else
        {
            ((wait(&status)) > 0);
        }
    }
    else
    {
        FILE *add = fopen("no.txt", "w");
        fprintf(add, "No");
        fclose(add);
    }
}

// int main
int main()
{
    // 1.a download files

    // 1.b unzip files into folders
    char *arg1[] = {"mkdir", "music", NULL};
    char *arg2[] = {"mkdir", "quote", NULL};
    makedir(arg1);
    makedir(arg2);

    pthread_t tid[2];
    int n[5];
    for (int i = 0; i < 2; i++)
    {
        n[i] = i + 1;
    }

    for (int i = 0; i < 2; i++)
    {
        pthread_create(&(tid[i]), NULL, &unzip, (void *)&n[i]);
    }
    for (int i = 0; i < 2; i++)
    {
        pthread_join(tid[i], NULL);
    }

    // 1.b decode files and output them

    pthread_t did[2];
    int j[5];
    for (int i = 0; i < 2; i++)
    {
        j[i] = i + 1;
    }

    for (int i = 0; i < 2; i++)
    {
        pthread_create(&(did[i]), NULL, &decode, (void *)&j[i]);
    }
    for (int i = 0; i < 2; i++)
    {
        pthread_join(did[i], NULL);
    }

    // char * data = "Hello World!";
    // long input_size = strlen(data);
    // char * encoded_data = base64_encode(data, input_size, &input_size);
    // printf("Encoded Data is: %s \n",encoded_data);

    // long decode_size = strlen(encoded_data);
    // char * decoded_data = base64_decode(encoded_data, decode_size, &decode_size);
    // printf("Decoded Data is: %s \n",decoded_data);

    // 1.c move result to new folder `hasil`
    char *fold[] = {"mkdir", "hasil", NULL};
    makedir(fold);

    minggat("music.txt");
    minggat("quote.txt");

    // 1.d zip `hasil` with password
    zip();

    // 1.e unzip `hasil.zip` and add `no.txt`
    // char *lagi[] = {"mkdir", "hasil", NULL};
    // makedir(lagi);

    pthread_t fid[2];
    int k[5];
    for (int i = 0; i < 2; i++)
    {
        k[i] = i + 1;
    }

    for (int i = 0; i < 2; i++)
    {
        pthread_create(&(fid[i]), NULL, &final, (void *)&k[i]);
    }
    for (int i = 0; i < 2; i++)
    {
        pthread_join(fid[i], NULL);
    }

    minggat("no.txt");
    zip();

    return 0;
    // gcc soal1.c -pthread -o soal1
}