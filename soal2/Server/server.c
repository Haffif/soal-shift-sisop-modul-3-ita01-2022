#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
pthread_t tid[3000];
int total = 0;
typedef struct file
{
  char problem_title[105], author[105], pathDesc[105], pathInput[105], pathOutput[105];
} problem;
typedef struct pengguna
{
  char name[105], password[105];
} akun;
bool same(char *a, char *b)
{
  for (int i = 0; a[i] != '\0'; i++)
    if (toupper(a[i]) != toupper(b[i]))
      return 0;
  return 1;
}
void storeProblemTSV(problem *soal, char *line)
{
  int i = 0, j = 0;
  while (line[i] != '\t')
    soal->problem_title[j++] = line[i++];
  soal->problem_title[j] = '\0';
  i++;
  j = 0;
  while (line[i])
    soal->author[j++] = line[i++];
  soal->author[j] = '\0';
  return;
}
void removeStr(char *a, char *b)
{
  char *match;
  int len = strlen(b);
  while ((match = strstr(a, b)))
    *match = '\0', strcat(a, match + len);
}
void *connectToClient(void *tmp)
{
  akun user;
  FILE *fp, *fp2, *fp3;
  char *regist = "register", *login = "login";
  char buffer[1100] = {0}, connected[1100] = "Connected", failed[1100] = "Other people are connected, please wait for them";
  int readerConn, serverSocket = *(int *)tmp;
  if (total == 1)
    send(serverSocket, connected, 1100, 0);
  else
    send(serverSocket, failed, 1100, 0);
  while (total > 1)
  {
    readerConn = read(serverSocket, buffer, 1100);
    if (total == 1)
      send(serverSocket, connected, 1100, 0);
    else
      send(serverSocket, failed, 1100, 0);
  }
  while (1)
  {
    readerConn = read(serverSocket, buffer, 1100);
    if (same(login, buffer))
    {
      readerConn = read(serverSocket, user.name, 1100);
      readerConn = read(serverSocket, user.password, 1100);
      fp3 = fopen("users.txt", "r");
      int check = 0;
      char *line = NULL;
      ssize_t len = 0;
      ssize_t fileReader;
      while ((fileReader = getline(&line, &len, fp3) != -1))
      {
        char userName[105], userPass[105];
        int i = 0, j = 0;
        while (line[i] != ':')
          userName[i] = line[i], i++;
        userName[i++] = '\0';
        while (line[i] != '\n')
          userPass[j] = line[i], j++, i++;
        userPass[j] = '\0';
        if (strcmp(user.name, userName) == 0 && strcmp(user.password, userPass) == 0)
        {
          check = 1, send(serverSocket, "LoginSuccess", strlen("LoginSuccess"), 0);
          break;
        }
      }
      if (check == 0)
        printf("Authorization for Login is Failed\n"), send(serverSocket, "LoginFail", strlen("LoginFail"), 0);
      else
      {
        printf("Authorization for Login is Success\n");
        while (1)
        {
          readerConn = read(serverSocket, buffer, 1100);
          if (same("logout", buffer))
            break;
          else if (same("add", buffer))
          {
            problem request;
            char basePath[1100], clientPath[1100], descPath[1100], inputPath[1100];
            char descServer[1100], inputServer[1100], outputServer[1100];
            readerConn = read(serverSocket, request.problem_title, 1100);
            printf("Judul problem: %s\n", request.problem_title);
            readerConn = read(serverSocket, request.pathDesc, 1100);
            printf("Filepath description.txt: %s\n", request.pathDesc);
            readerConn = read(serverSocket, request.pathInput, 1100);
            printf("Filepath input.txt: %s\n", request.pathInput);
            readerConn = read(serverSocket, request.pathOutput, 1100);
            printf("Filepath output.txt: %s\n", request.pathOutput);
            strcpy(basePath, "problems/");
            strcat(basePath, request.problem_title);
            mkdir(basePath, 0777);
            strcat(basePath, "/");
            strcpy(descServer, basePath);
            strcat(descServer, "description.txt");
            strcpy(inputServer, basePath);
            strcat(inputServer, "input.txt");
            strcpy(outputServer, basePath);
            strcat(outputServer, "output.txt");
            srand(time(0));
            int gachaTestcase = rand() % 50, gachaInput;
            if (gachaTestcase < 10)
              gachaTestcase += 5;
            FILE *data;
            if (!(data = fopen(descServer, "r")))
              data = fopen(descServer, "a+"),
              fprintf(data, "%s by %s\nDescription:\n", request.problem_title, user.name),
              fprintf(data, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lorem leo, malesuada in ultricies ac, volutpat id dui.\n"),
              fprintf(data, "Filepath description.txt: %s\nFilepath input.txt: %s\nFilepath output.txt: %s\n", request.pathDesc, request.pathInput, request.pathOutput),
              fclose(data);
            if (!(data = fopen(inputServer, "r")))
            {
              data = fopen(inputServer, "a+");
              fprintf(data, "%d\n", gachaTestcase);
              for (int i = 0; i < gachaTestcase; i++)
                gachaInput = rand() % 150, fprintf(data, "%d ", gachaInput);
              fprintf(data, "\n");
              fclose(data);
            }
            int res[gachaTestcase], gachaOutput;
            if (!(data = fopen(outputServer, "r")))
            {
              data = fopen(outputServer, "a+");
              for (int i = 0; i < gachaTestcase; i++)
                gachaOutput = rand() % 1000, res[i] = gachaOutput, fprintf(data, "Case #%d: %d\n", i + 1, gachaOutput);
              fclose(data);
            }
            char gachaTC[1100];
            sprintf(gachaTC, "%d", gachaTestcase);
            send(serverSocket, gachaTC, sizeof(gachaTC), 0);
            int gachaOut = rand() % 500, gachaResult = 0;
            if (gachaOut % 2 == 0)
              gachaResult = 1; // kalau genap AC, ganjil WA (nilai default)
            char outputForClient[1100];
            sprintf(outputForClient, "Case #1: %d\n", gachaResult ? res[0] : res[0] + 5);
            send(serverSocket, outputForClient, 1100, 0);
            memset(outputForClient, 0, sizeof(outputForClient));
            for (int i = 1; i < gachaTestcase; i++)
              sprintf(outputForClient, "Case #%d: %d\n", i + 1, res[i]),
                  send(serverSocket, outputForClient, 1100, 0),
                  memset(outputForClient, 0, sizeof(outputForClient));
            fp = fopen("problems.tsv", "a");
            fprintf(fp, "%s\t%s\n", request.problem_title, user.name), fclose(fp);
            printf("%s is adding new problem called %s to the server.\n", user.name, request.problem_title);
            continue;
          }
          else if (same("download", buffer))
          {
            readerConn = read(serverSocket, buffer, 1100);
            bool foundDesc = false, foundInput = false;
            char pathDownload[1100] = "problems/", descPath[1100], descTarget[1100];
            strcat(pathDownload, buffer);
            strcpy(descPath, pathDownload);
            strcat(descPath, "/description.txt");
            char inputPath[1100], inputTarget[1100], outputPath[1100], outputTarget[1100];
            strcpy(inputPath, pathDownload);
            strcat(inputPath, "/input.txt");
            strcpy(outputPath, pathDownload);
            strcat(outputPath, "/output.txt");
            char foundSuccess[] = "File is ready to be downloaded.\n", notFound[] = "No such file found.\n";
            fp = fopen(descPath, "r");
            FILE *source, *target;
            source = fopen(descPath, "r");
            char *line = NULL, *line2 = NULL, *line3 = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            int num = 0, fd, readLength;
            while ((fileReader = getline(&line, &len, source) != -1))
            {
              num++;
              if (num > 3)
              {
                char pathContent[105];
                int i = 0, j = 0;
                while (line[i] != ':')
                  i++;
                line[i] = '\0';
                i += 2;
                while (line[i] != '\n')
                  pathContent[j++] = line[i++];
                pathContent[j] = '\0';
                if (num == 4)
                  strcpy(descTarget, pathContent);
                if (num == 5)
                  strcpy(inputTarget, pathContent);
                if (num == 6)
                  strcpy(outputTarget, pathContent);
              }
            }
            int numLine = 0;
            FILE *fpSource = fopen(inputPath, "r");
            ssize_t len2 = 0;
            ssize_t fileReader2;
            bool checkSubmission = true;
            while ((fileReader2 = getline(&line2, &len2, fpSource) != -1))
              numLine++;
            fclose(fpSource);
            fpSource = fopen(inputPath, "r");
            char buf[1100];
            sprintf(buf, "%d", numLine);
            send(serverSocket, buf, 1100, 0);
            send(serverSocket, inputTarget, 1100, 0);
            ssize_t len3 = 0;
            ssize_t fileReader3;
            while ((fileReader3 = getline(&line3, &len3, fpSource) != -1))
            {
              char outputSource[105];
              int i = 0;
              while (line3[i] != '\n')
                outputSource[i] = line3[i], i++;
              outputSource[i] = '\0';
              send(serverSocket, outputSource, 1100, 0);
            }
            fclose(fpSource);
            if (fp)
              foundDesc = true;
            if (!foundDesc)
              send(serverSocket, notFound, 1100, 0);
            else
            {
              send(serverSocket, foundSuccess, 1100, 0);
              send(serverSocket, descTarget, 1100, 0);
              fd = open(descPath, O_RDONLY);
              if (!fd)
                perror("Fail to open"), exit(EXIT_FAILURE);
              while (1)
              {
                memset(descPath, 0x00, 1100);
                readLength = read(fd, descPath, 1100);
                if (readLength == 0)
                  break;
                else
                  send(serverSocket, descPath, readLength, 0);
              }
              close(fd);
            }
            fclose(fp);
          }
          else if (same("submit", buffer))
          {
            char problemName[1100], pathSource[1100] = "problems/";
            readerConn = read(serverSocket, problemName, 1100);
            strcat(pathSource, problemName);
            strcat(pathSource, "/output.txt");
            FILE *fpSource = fopen(pathSource, "r");
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            bool checkSubmission = true;
            while (fileReader = getline(&line, &len, fpSource) != -1)
            {
              char outputSource[105], outputSubmission[105];
              int i = 0, j = 0;
              while (line[i] != ':')
                i++;
              line[i++] = '\0';
              while (line[i] != '\n')
                outputSource[j++] = line[i++];
              outputSource[j] = '\0';
              readerConn = read(serverSocket, outputSubmission, 105);
              int source = atoi(outputSource), submit = atoi(outputSubmission);
              printf("[%s] source: %d dan submission: %d\n", submit == source ? "AC" : "WA", source, submit);
              if (submit != source)
                checkSubmission = false;
            }
            printf("%s is %s on problem %s!\n", user.name, checkSubmission ? "accepted (AC)" : "having a wrong answer (WA)", problemName);
            send(serverSocket, checkSubmission ? "AC\nAccepted in Every Testcases\n" : "WA\nWA in the First Testcase\n", 1100, 0);
          }
          else if (same("see", buffer))
          {
            fp = fopen("problems.tsv", "r");
            if (!fp)
            {
              send(serverSocket, "e", sizeof("e"), 0);
              memset(buffer, 0, sizeof(buffer));
              continue;
            }
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            while ((fileReader = getline(&line, &len, fp) != -1))
            {
              problem temp_problem;
              storeProblemTSV(&temp_problem, line);
              char message[1100];
              sprintf(message, "%s by %s", temp_problem.problem_title, temp_problem.author);
              send(serverSocket, message, 1100, 0);
            }
            send(serverSocket, "e", sizeof("e"), 0), fclose(fp);
            memset(buffer, 0, sizeof(buffer));
          }
        }
      }
      fclose(fp3);
    }
    else if (same(regist, buffer))
    {
      fp2 = fopen("users.txt", "a"), fp3 = fopen("users.txt", "r");
      readerConn = read(serverSocket, user.name, 1100),
      readerConn = read(serverSocket, user.password, 1100);
      int flag = 0;
      char *line = NULL;
      ssize_t len = 0;
      ssize_t fileReader;
      int check = 0, numUsers = 0;
      while ((fileReader = getline(&line, &len, fp3) != -1))
      {
        numUsers++;
        char userName[105], userPass[105];
        int i = 0;
        while (line[i] != ':')
          userName[i] = line[i], i++;
        userName[i] = '\0';
        i++;
        int j = 0;
        while (line[i] != '\n')
          userPass[j] = line[i], j++, i++;
        userPass[j] = '\0';
        if (strcmp(user.name, userName) == 0)
        {
          check = 0;
          break;
        }
        int kapital = 0, lower = 0, angka = 0, jumlah = 0;
        for (int i = 0; i < strlen(user.password); i++)
        {
          if (user.password[i] >= 48 && user.password[i] <= 57)
            angka = 1;
          if (user.password[i] >= 65 && user.password[i] <= 90)
            kapital = 1, jumlah++;
          if (user.password[i] >= 97 && user.password[i] <= 122)
            lower = 1, jumlah++;
        }
        if (!angka || !lower || !kapital || jumlah < 6)
          break;
        if (angka && lower && kapital && jumlah >= 6)
          check = 1;
      }
      if (check || numUsers == 0)
      {
        fprintf(fp2, "%s:%s\n", user.name, user.password);
        send(serverSocket, "Register berhasil", strlen("Register berhasil"), 0);
      }
      else
        send(serverSocket, "RegError", strlen("RegError"), 0);
      fclose(fp2);
    }
    else if (same("exit", buffer) || same("3", buffer))
    {
      close(serverSocket), total--;
      break;
    }
  }
}

int main(int argc, char const *argv[])
{
  struct sockaddr_in alamat;
  int serverSocket, serverFD, readerConn, opt = 1, jumlahKoneksi = 5, porter = 8080, addr_len = sizeof(alamat);
  if ((serverFD = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    perror("socket failed"), exit(EXIT_FAILURE);
  if (setsockopt(serverFD, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    perror("setsockopt"), exit(EXIT_FAILURE);
  alamat.sin_family = AF_INET, alamat.sin_addr.s_addr = INADDR_ANY, alamat.sin_port = htons(porter);
  if (bind(serverFD, (struct sockaddr *)&alamat, sizeof(alamat)) < 0)
    perror("bind failed"), exit(EXIT_FAILURE);
  if (listen(serverFD, jumlahKoneksi) < 0)
    perror("listen"), exit(EXIT_FAILURE);
  mkdir("problems", 0777);
  FILE *fp = fopen("problems.tsv", "a");
  if (!fp)
    fprintf(fp, "\n"), fclose(fp);
  fclose(fp);
  while (1)
  {
    if ((serverSocket = accept(serverFD, (struct sockaddr *)&alamat, (socklen_t *)&addr_len)) < 0)
      perror("accept"), exit(EXIT_FAILURE);
    pthread_create(&(tid[total]), NULL, &connectToClient, &serverSocket), total++;
  }
  return 0;
}