# soal-shift-sisop-modul-3-ITA01-2022

Hasil Pengerjaan Soal Shift Praktikum SISTEM OPERASI 2022<br>
Anggota Kelompok:<br>
<ol>
<li>Haffif Rasya Fauzi (5027201002)</li>
<li>Muhammad Faris Anwari (5027201008)</li>
<li>Muhammad Dzakwan (5027201065)</li>
</ol>

---
## Soal 1

Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.<br>
<ol type="a">
    <li>Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.</li>
    <li>Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.</li>
    <li>Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.</li>
    <li>Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)</li>
    <li>Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.</li>
</ol>

Catatan:
- Boleh menggunakan execv(), tapi bukan mkdir, atau system
- Jika ada pembenaran soal, akan di-update di catatan
- Kedua file .zip berada di folder modul
- Nama user di passwordnya adalah nama salah satu anggota kelompok, baik yang mengerjakan soal tersebut atau tidak. Misalkan satu kelompok memiliki anggota namanya Yudhistira, Werkudara, dan Janaka. Yang mengerjakan adalah Janaka. Nama passwordnya bisa mihinomenestyudhistira, mihinomenestwerkudara, atau mihinomenestjanaka
- Menggunakan apa yang sudah dipelajari di Modul 3 dan, kalau  perlu, di Modul 1 dan 2

### Penyelesaian

Kode:<br>
[Source code](./soal1/)

#### 1.a Unduh dan unzip
Praktikan mendefinisikan fungsi `makedir` untuk membuat folder musik dan hasil.

```c#
void makedir(char *arg[])
{
    int status;
    pid_t child;
    child = fork();
    if (child == 0)
    {
        execv("/bin/mkdir", arg);
    }
    else
    {
        ((wait(&status)) > 0);
    }
}
```

Praktikan mendefinisikan fungsi `unzip`.

```c#
void *unzip(void *arg)
{
    int *n = (int *)arg;
    int num = *n;
    char *mus[] = {"unzip", "-qo", "music.zip", "-d", "/home/ubuntu/sisop/m3s1/music", NULL};
    char *quo[] = {"unzip", "-qo", "quote.zip", "-d", "/home/ubuntu/sisop/m3s1/quote", NULL};

    int status;
    pid_t child;
    if (num == 1)
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/unzip", mus);
        }
        else
        {
            ((wait(&status)) > 0);
        }
    }
    else
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/unzip", quo);
        }
        else
        {
            ((wait(&status)) > 0);
        }
    }
}
```
- Fungsi `unzip` menerima argumen integer `n`. Bila `n == 1`, fungsi akan mengunzip music, dan bila `n == 2`, fungsi mengunzip quote

Praktikan memanggil fungsi-fungsi di atas, adapun `unzip` dipanggil secara multithreading agar berjalan secara bersamaan.

```c#
char *arg1[] = {"mkdir", "music", NULL};
char *arg2[] = {"mkdir", "quote", NULL};
makedir(arg1);
makedir(arg2);

pthread_t tid[2];
int n[5];
for (int i = 0; i < 2; i++)
{
    n[i] = i + 1;
}

for (int i = 0; i < 2; i++)
{
    pthread_create(&(tid[i]), NULL, &unzip, (void *)&n[i]);
}
for (int i = 0; i < 2; i++)
{
    pthread_join(tid[i], NULL);
}
```

#### 1.b Decode base64
Praktikan menulis pre-process: fungsi-fungsi, matriks-matriks dan variabel yang akan digunakan untuk proses decoding.

```c#
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};

void build_decoding_table()
{

    decoding_table = malloc(256);

    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char)encoding_table[i]] = i;
}

void base64_cleanup()
{
    free(decoding_table);
}

char *base64_encode(const unsigned char *data,
                    size_t input_length,
                    size_t *output_length)
{

    *output_length = 4 * ((input_length + 2) / 3);

    char *encoded_data = malloc(*output_length);
    if (encoded_data == NULL)
        return NULL;

    for (int i = 0, j = 0; i < input_length;)
    {

        uint32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (int i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';

    return encoded_data;
}

unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length)
{

    if (decoding_table == NULL)
        build_decoding_table();

    if (input_length % 4 != 0)
        return NULL;

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=')
        (*output_length)--;
    if (data[input_length - 2] == '=')
        (*output_length)--;

    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL)
        return NULL;

    for (int i = 0, j = 0; i < input_length;)
    {

        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6) + (sextet_b << 2 * 6) + (sextet_c << 1 * 6) + (sextet_d << 0 * 6);

        if (j < *output_length)
            decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length)
            decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length)
            decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}
```

Kemudian, praktikan mendefinisikan fungsi `decode` yang akan dipanggil secara multithreading nanti.

```c#
void *decode(void *arg)
{
    int *n = (int *)arg;
    int num = *n;

    if (num == 1)
    {
        // printf("Data from music:");
        FILE *mout = fopen("music.txt", "w");
        for (int i = 1; i < 10; i++)
        {
            char c[100];
            FILE *fptr;
            char path[20] = "./music/m";
            char temp[20];
            sprintf(temp, "%d.txt", i);
            strcat(path, temp);
            // printf("\n%s\n", path);
            if ((fptr = fopen(path, "r")) == NULL)
            {
                printf("Error! File cannot be opened.");
                // Program exits if the file pointer returns NULL.
                exit(1);
            }
            // reads text until newline is encountered
            fscanf(fptr, "%[^\n]", c);
            long decode_size = strlen(c);
            char *plain = base64_decode(c, decode_size, &decode_size);
            fprintf(mout, "%s\n", plain);
            fclose(fptr);
        }
        fclose(mout);
    }
    else
    {
        // printf("Data from quote:");
        FILE *qout = fopen("quote.txt", "w");
        for (int i = 1; i < 10; i++)
        {
            char c[100];
            FILE *fptr;
            char path[20] = "./quote/q";
            char temp[20];
            sprintf(temp, "%d.txt", i);
            strcat(path, temp);
            // printf("\n%s\n", path);
            fptr = fopen(path, "r");
            // reads text until newline is encountered
            fscanf(fptr, "%[^\n]", c);
            long decode_size = strlen(c);
            char *plain = base64_decode(c, decode_size, &decode_size);
            fprintf(qout, "%s\n", plain);
            fclose(fptr);
        }
        fclose(qout);
    }
}
```
- Fungsi menerima argumen integer `n`, jika `n == 1` maka fungsi akan memproses folder music, jika `n == 2` maka folder quote.
- Praktikan mengiterasi file-file yang ada dalam tiap folder dengan cara membuat for loop `for (int i = 1; i < 10; i++)`, lalu menyusun path tiap file sebagai berikut:

```c#
FILE *fptr;
char path[20] = "./quote/q";
char temp[20];
sprintf(temp, "%d.txt", i);
strcat(path, temp);
// printf("\n%s\n", path);
fptr = fopen(path, "r");
```

- Praktikan membaca tiap file dan menyimpannya dalam string `c` yang telah dideclare sebelumnya

```c#
char c[100];
...
long decode_size = strlen(c);
char *plain = base64_decode(c, decode_size, &decode_size);
```

- Praktikan mengoutputkan hasil decoding ke file .txt sesuai folder masing-masing. Tiap kalimat dipisah dengan newline. Dibawah adalah contoh untuk output folder quote:

```c#
FILE *qout = fopen("quote.txt", "w");
...
fprintf(qout, "%s\n", plain);
fclose(fptr);
```

Kembali dalam `int main()`, praktikan memanggil fungsi `decode` secara multithreading.

```c#
pthread_t did[2];
    int j[5];
    for (int i = 0; i < 2; i++)
    {
        j[i] = i + 1;
    }

    for (int i = 0; i < 2; i++)
    {
        pthread_create(&(did[i]), NULL, &decode, (void *)&j[i]);
    }
    for (int i = 0; i < 2; i++)
    {
        pthread_join(did[i], NULL);
    }
```

#### 1.c Pindah hasil decoding
Praktikan membuat folder `hasil` dan memanggil fungsi `minggat` yang akan memindahkan tiap .txt ke folder `hasil`.

```c#
char *fold[] = {"mkdir", "hasil", NULL};
makedir(fold);

minggat("music.txt");
minggat("quote.txt");
```

Adapun isi fungsi `minggat` ialah sebagai berikut:

```c#
void minggat(char file[])
{
    int status;
    pid_t child;
    child = fork();
    char bash[] = "mv";
    char target[] = "./hasil";
    if (child == 0)
    {
        execlp(bash, bash, file, target, NULL);
    }
    else
    {
        ((wait(&status)) > 0);
    }
}
```

#### 1.d Zip folder hasil
Praktikan memanggil fungsi `zip`. Adapun isi fungsi `zip` ialah sebagai berikut:

```c#
void zip()
{
    int status;
    pid_t child;
    child = fork();
    if (child == 0)
    {
        char *argv[] = {"zip", "-r", "-m", "-q", "hasil.zip", "-P", "mihinomenestfaris", "hasil", NULL};
        execv("/bin/zip", argv);
    }
    else
    {
        ((wait(&status)) > 0);
    }
}
```
- Hasil zip terkunci sesuai dengan kriteria soal, yakni dengan password `mihinomenest[nama]`.

#### 1.e Tambah isi folder hasil
Praktikan mendefinisikan fungsi `final` yang akan melakukan dua hal sekaligus:
1. Mengunzip *hasil.zip*
2. Menulis *no.txt*

```c#
void * final(void *arg)
{
    int *n = (int *)arg;
    int num = *n;

    char *hasil[] = {"unzip", "-P", "mihinomenestfaris", "-qo", "hasil.zip", "-d", "/home/ubuntu/sisop/m3s1/hasil", NULL};
    int status;
    pid_t child;
    if (num == 1)
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/unzip", hasil);
        }
        else
        {
            ((wait(&status)) > 0);
        }
    }
    else
    {
        FILE *add = fopen("no.txt", "w");
        fprintf(add, "No");
        fclose(add);
    }
}
```

Praktikan kemudian memanggil fungsi `final` secara multithreading. Terakhir, praktikan memanggil fungsi `minggat` untuk memindahkan *no.txt* ke dalam folder hasil dan fungsi `zip` untuk mengzip ulang folder hasil.

```c#
pthread_t fid[2];
int k[5];
for (int i = 0; i < 2; i++)
{
    k[i] = i + 1;
}

for (int i = 0; i < 2; i++)
{
    pthread_create(&(fid[i]), NULL, &final, (void *)&k[i]);
}
for (int i = 0; i < 2; i++)
{
    pthread_join(fid[i], NULL);
}

minggat("no.txt");
zip();
```

Output:
- *music.txt* dan *quote.txt*

![hasil decoding](./img/music.PNG)

![hasil decoding](./img/quote.PNG)

- Tampak akhir isi zip hasil

![isi hasil](./img/hasil.PNG)

### Kendala
Praktikan terkendala ketika mencoba mengiterasi tiap file yang perlu didecode dalam folder music/quote. Kendala terselesaikan setelah praktikan mengalokasikan ruang yang cukup pada string `path` yang menampung path file yang akan diproses.

---
## Soal 2

Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:<br>
<ol type="a">
    <li>Melakukan login dan register</li>
    <li>Membuat database yang bernama problems.tsv</li>
    <li>Membuat command yang bernama add</li>
    <li>membuat command yang bernama see</li>
    <li>Membuat command yang bernama download</li>
    <li>Membuat command yang bernama submit</li>
    <li>Membuat server dapat melakukan multiple-connection</li>
    <li></li>
</ol>

Catatan:
- Dilarang menggunakan system() dan execv(). Silahkan dikerjakan sepenuhnya dengan thread dan socket programming. 
- Untuk download dan upload silahkan menggunakan file teks dengan ekstensi dan isi bebas (yang ada isinya bukan touch saja dan tidak kosong)
- Untuk error handling jika tidak diminta di soal tidak perlu diberi.

### Penyelesaian

Kode:<br>
[Source code](./soal2/)

#### 2.a Login dan register
Pada saat client akan dijalankan, client terlebih dahulu melakukan koneksi dengan server menggunakan socket. Terdapat beberapa case yang dapat terjadi dalam menghubungkan antara client dan server seperti kegagalan dalam pembuatan socket sehingga akan mengirimkan pesan "Failed to create socket". Kemudian, kegagalan dalam alamat address yang akan mengirimkan pesan "Connection failed". Apabila koneksi telah berhasil dibuat akan muncul pesan untuk mengetikkan sesuatu agar server bisa memastikan bahwa koneksi server dan client telah terhubung. Untuk kodenya yaitu sebagai berikut:

```c#
struct sockaddr_in alamatServer;
  char *login = "login", *logout = "logout", *regist = "register";
  char temp, buffer[1100] = {0}, cmd[1100], username[1100], password[1100];
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf(Failed to create socket);
    return -1;
  }
  memset(&alamatServer, '0', sizeof(alamatServer));
  alamatServer.sin_family = AF_INET, alamatServer.sin_port = htons(port);
  if (inet_pton(AF_INET, "127.0.0.1", &alamatServer.sin_addr) <= 0)
  {
    printf("Invalid address\n");
    return -1;
  }
  if (connect(sock, (struct sockaddr *)&alamatServer, sizeof(alamatServer)) < 0)
  {
    printf("Connection failed\n");
    return -1;
  }
  readerConn = read(sock, buffer, 1100);
  printf("%s\n", buffer);
  while (!same("Connected", buffer))
  {
    printf("Enter anything to check whether you can connect to the server or not.\n");
    scanf("%s", cmd);
    send(sock, cmd, 1100, 0);
    readerConn = read(sock, buffer, 1100);
    printf("%s\n", buffer);
  }
  if (!same("Connected", buffer))
    return 0;
```

Setelah memastikan bahwa koneksi telah terjalin maka client akan diberikan beberapa pilihan seperti login, register, dan exit. command scanf berfungsi untuk mengambil inputan dari client yang tadi telah dipilih dan akan menjalankan fungsi selanjutnya. Untuk kodenya yaitu sebagai berikut:

```c#
while (1)
  {
    printf("1. Login\n2. Register\n3. Exit\n");
    scanf("%s", cmd);
```

Apabila client memasukkkan command register akan muncul pesan untuk memasukkan username dan password dengan menggunakan command scanf untuk mengirimkan data-data tersebut kepada server. Lalu, server nanti akan mengirimkan "RegError" atau "Register berhasil" dari data yang diinputkan client. Apabila mengembalikan "RegError" maka akan menampilkan pesan bahwa username tersebut sudah ada atau tersedia atau password yang tidak sesuai ketentuan. Apabila mengembalikan "Register berhasil" maka akan menampilkan pesan bahwa registrasi sudah berhasil. Untuk kodenya yaitu sebagai berikut:

```c#
else if (same(cmd, regist))
    {
      send(sock, regist, strlen(regist), 0);
      memset(buffer, 0, sizeof(buffer));
      printf("Register\nUsername: ");
      scanf("%c", &temp);
      scanf("%[^\n]", username);
      send(sock, username, 1100, 0);
      printf("Password: ");
      scanf("%c", &temp);
      scanf("%[^\n]", password);
      send(sock, password, 1100, 0);
      readerConn = read(sock, buffer, 1100);
      if (same(buffer, "RegError"))
        printf("Register gagal, username sudah terambil atau password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil.\n");
      if (same(buffer, "Register berhasil"))
        printf("Register berhasil\n");
      memset(buffer, 0, sizeof(buffer));
    }
```

Pada sisi server akan membuka file users.txt dengan metode "a" dan "r" untuk melakukan append dan membaca file users.txt. Kemudian server akan menerimadata registrasi serta akan melakukan pengecekan antara username yang ada dalam file users.txt dengan username yang diterima dari client. Sehingga apabila username telah ada dan server akan melakukan break pada operasi dan mengembalikan "RegError". Apabila pengecekan username telah berhasil maka akan dilewati tanpa break. Kemudian akan dilanjutkan pemeriksaan terhadap data password yang telah ditentukan seperti, huruf kapital, huruf kecil, terdapat angka, dan minimal 6 huruf dengan menggunakan nilai ASCII. Jika syarat-syarat terpenuhi maka akan mengembalikan "Register berhasil" pada client. Untuk kode yaitu sebagai berikut:

```c#
    else if (same(regist, buffer))
    {
      fp2 = fopen("users.txt", "a"), fp3 = fopen("users.txt", "r");
      readerConn = read(serverSocket, user.name, 1100),
      readerConn = read(serverSocket, user.password, 1100);
      int flag = 0;
      char *line = NULL;
      ssize_t len = 0;
      ssize_t fileReader;
      int check = 0, numUsers = 0;
      while ((fileReader = getline(&line, &len, fp3) != -1))
      {
        numUsers++;
        char userName[105], userPass[105];
        int i = 0;
        while (line[i] != ':')
          userName[i] = line[i], i++;
        userName[i] = '\0';
        i++;
        int j = 0;
        while (line[i] != '\n')
          userPass[j] = line[i], j++, i++;
        userPass[j] = '\0';
        if (strcmp(user.name, userName) == 0)
        {
          check = 0;
          break;
        }
        int kapital = 0, lower = 0, angka = 0, jumlah = 0;
        for (int i = 0; i < strlen(user.password); i++)
        {
          if (user.password[i] >= 48 && user.password[i] <= 57)
            angka = 1;
          if (user.password[i] >= 65 && user.password[i] <= 90)
            kapital = 1, jumlah++;
          if (user.password[i] >= 97 && user.password[i] <= 122)
            lower = 1, jumlah++;
        }
        if (!angka || !lower || !kapital || jumlah < 6)
          break;
        if (angka && lower && kapital && jumlah >= 6)
          check = 1;
      }
      if (check || numUsers == 0)
      {
        fprintf(fp2, "%s:%s\n", user.name, user.password);
        send(serverSocket, "Register berhasil", strlen("Register berhasil"), 0);
      }
      else
        send(serverSocket, "RegError", strlen("RegError"), 0);
      fclose(fp2);
    }
    else if (same("exit", buffer) || same("3", buffer))
    {
      close(serverSocket), total--;
      break;
    }
  }
```

Selanjutnya, apabila  client memasukkan command login, maka akan muncul pesan agar client memasukkan data username dan password yang akan dikirimkan kepada server. Jika dikembalikan "LoginSuccess" dari server, client akan dapat masuk pada tahap berikutnya dan pesan bahwa login berhasil akan ditampilkan. Namun, jika tidak dikembalikan "LoginSuccess" dari server, akan ditampilkan pesan bahwa proses login telah gagal dan client dapat mencoba untuk memasukkan kembali informasinya. Untuk kodenya yaitu sebagai berikut:

```c#
 while (1)
  {
    if (same(cmd, login))
    {
      send(sock, login, strlen(login), 0);
      printf("Username: ");
      scanf("%c", &temp);
      scanf("%[^\n]", username);
      send(sock, username, 1100, 0);
      printf("Password: ");
      scanf("%c", &temp);
      scanf("%[^\n]", password);
      send(sock, password, 1100, 0);
      memset(buffer, 0, sizeof(buffer));
      readerConn = read(sock, buffer, 1100);
      if (same(buffer, "LoginSuccess"))
        printf("Login process is success! Welcome aboard! ^o^\n");
      else
        printf("Login process failed, please try again.\n");
```

Pada sisi server akan membaca data login yang dikirimkan oleh klien dan membuka file user.txt dengan fopen mode "r" untuk membaca data user yang tersedia. Server akan membaca file pada users.txt dan melakukan compare terhadap data login yang telah diberikan oleh user untuk menentukan apakah user berhasil login atau tidak. Jika ditemukan data login yang valid, nilai check akan menjadi 1 dan server akan mengirimkan "LoginSuccess" sehingga client akan dapat melanjutkan ke tahap berikutnya. Untuk kodenya yaitu sebagai berikut:

```c#
  while (1)
  {
    readerConn = read(serverSocket, buffer, 1100);
    if (same(login, buffer))
    {
      readerConn = read(serverSocket, user.name, 1100);
      readerConn = read(serverSocket, user.password, 1100);
      fp3 = fopen("users.txt", "r");
      int check = 0;
      char *line = NULL;
      ssize_t len = 0;
      ssize_t fileReader;
      while ((fileReader = getline(&line, &len, fp3) != -1))
      {
        char userName[105], userPass[105];
        int i = 0, j = 0;
        while (line[i] != ':')
          userName[i] = line[i], i++;
        userName[i++] = '\0';
        while (line[i] != '\n')
          userPass[j] = line[i], j++, i++;
        userPass[j] = '\0';
        if (strcmp(user.name, userName) == 0 && strcmp(user.password, userPass) == 0)
        {
          check = 1, send(serverSocket, "LoginSuccess", strlen("LoginSuccess"), 0);
          break;
        }
      }
```

Akan tetapi, apabila informasi login tidak sesuai dengan data yang dimiliki server, nilai check akan tetap 0 dan server akan mengirimkan "LoginFail" kepada client sehingga client harus memasukkan kembali data login. Untuk kodenya yaitu sebagai berikut:

```c#
      if (check == 0)
        printf("Authorization for Login is Failed\n"), send(serverSocket, "LoginFail", strlen("LoginFail"), 0);
      else
      {
        printf("Authorization for Login is Success\n");
```

Untuk dokumentasi login dan register yaitu sebagai berikut:

- Client gagal register
![gagal register](./img/register1.png)

- CLient berhasil register
![berhasil register](./img/register2.png)

- Client gagal login
![gagal login](./img/login1.png)

- Client berhasil login
![berhasil register](./img/login2.png)

#### 2.b Membuat database

Jika server telah berhasil dijalankan, maka akan dieksekusi fungsi fopen metode "a" yang akan membuat file problems.tsv jika sebelumnya file tersebut belum ada. Untuk kodenya yaitu sebagai berikut:

```c#
int main(int argc, char const *argv[])
{
  struct sockaddr_in alamat;
  int serverSocket, serverFD, readerConn, opt = 1, jumlahKoneksi = 5, porter = 8080, addr_len = sizeof(alamat);
  if ((serverFD = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    perror("socket failed"), exit(EXIT_FAILURE);
  if (setsockopt(serverFD, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    perror("setsockopt"), exit(EXIT_FAILURE);
  alamat.sin_family = AF_INET, alamat.sin_addr.s_addr = INADDR_ANY, alamat.sin_port = htons(porter);
  if (bind(serverFD, (struct sockaddr *)&alamat, sizeof(alamat)) < 0)
    perror("bind failed"), exit(EXIT_FAILURE);
  if (listen(serverFD, jumlahKoneksi) < 0)
    perror("listen"), exit(EXIT_FAILURE);
  mkdir("problems", 0777);
  FILE *fp = fopen("problems.tsv", "a");
```

Client dapat menambahkan problem pada file ini dengan command add yang akan dijelaskan pada point C. File problems.tsv nanti akan berisi judul problem dan username pembuat problem yang dipisahkan oleh \t sesuai perintah soal.Untuk kodenya yaitu sebagai berikut:

```c#
  p = fopen("problems.tsv", "a");
  fprintf(fp, "%s\t%s\n", request.problem_title, user.name), fclose(fp);
```

Untuk dokumentasi problems.tsv yaitu sebagai berikut:

![problems.tsv](./img/database.png)

#### 2.c Membuat command add

Setelah client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan add problem. Untuk kodenya yaitu sebagai berikut:

```c#
 if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```

Jika client memasukkan command add, akan muncul pesan untuk memasukkan judul problem yang akan diambil menggunakan fungsi scanf dan dikirimkan kepada server. Kemudian akan muncul juga pesan untuk memasukkan filepath description.txt yang berisi deskripsi problem, filepath input.txt yang berisi input testcase untuk menyelesaikan problem, dan output.txt yang akan digunakan untuk melakukan pengecekan pada submission client. Untuk kodenya yaitu sebagai berikut:

```c#
          else if (same(cmd, "add"))
          {
            send(sock, cmd, 1100, 0);
            char data[1100], problemName[1100];
            printf("Judul Problem: ");
            scanf("\n%[^\n]%*c", problemName);
            send(sock, problemName, 1100, 0);
            printf("Filepath description.txt: ");
            scanf("\n%[^\n]%*c", data);
            char outputFolder[1100], inputFolder[1100], descFolder[1100], outputPath[1100];
            strcpy(descFolder, data);
            removeStr(descFolder, "/description.txt");
            mkdir(descFolder, 0777);
            send(sock, data, 1100, 0);
            printf("Filepath input.txt: ");
            scanf("\n%[^\n]%*c", data);
            strcpy(inputFolder, data);
            removeStr(inputFolder, "/input.txt");
            mkdir(inputFolder, 0777);
            send(sock, data, 1100, 0);
            printf("Filepath output.txt: ");
            scanf("\n%[^\n]%*c", data);
            send(sock, data, 1100, 0);
            readerConn = read(sock, buffer, 1100);
            int numGacha = atoi(buffer);
            memset(buffer, 0, sizeof(buffer));
            strcpy(outputPath, data);
            FILE *fileManager;
            fileManager = fopen(outputPath, "a+");
            for (int i = 0; i < numGacha; i++)
              readerConn = read(sock, buffer, 1100),
              fprintf(fileManager, "%s", buffer),
              memset(buffer, 0, sizeof(buffer));
            fclose(fileManager);
            printf("Successfully adding problem %s to the server!\n", problemName);
            continue;
          }
```

Pada sisi server, server nanti akan membaca kiriman data-data dari client dan mengirimakn request untuk judul problem, path description, path input, dan path output. Server kemudian membuat direktori yang mengambil nama dari input judul problem yang dimasukkan oleh client dan menyimpan problem di dalamnya dengan strcpy dan strcat. Setelah itu, akan ditampilkan pesan "(username) is adding new problem called (judul) to the server". Untuk kodenya yaitu sebagai berikut:

```c#
          else if (same("add", buffer))
          {
            problem request;
            char basePath[1100], clientPath[1100], descPath[1100], inputPath[1100];
            char descServer[1100], inputServer[1100], outputServer[1100];
            readerConn = read(serverSocket, request.problem_title, 1100);
            printf("Judul problem: %s\n", request.problem_title);
            readerConn = read(serverSocket, request.pathDesc, 1100);
            printf("Filepath description.txt: %s\n", request.pathDesc);
            readerConn = read(serverSocket, request.pathInput, 1100);
            printf("Filepath input.txt: %s\n", request.pathInput);
            readerConn = read(serverSocket, request.pathOutput, 1100);
            printf("Filepath output.txt: %s\n", request.pathOutput);
            strcpy(basePath, "problems/");
            strcat(basePath, request.problem_title);
            mkdir(basePath, 0777);
            strcat(basePath, "/");
            strcpy(descServer, basePath);
            strcat(descServer, "description.txt");
            strcpy(inputServer, basePath);
            strcat(inputServer, "input.txt");
            strcpy(outputServer, basePath);
            strcat(outputServer, "output.txt");
            fp = fopen("problems.tsv", "a");
            fprintf(fp, "%s\t%s\n", request.problem_title, user.name), fclose(fp);
            printf("%s is adding new problem called %s to the server.\n", user.name, request.problem_title);
            continue;
          }
```

Untuk dokumentasi command add yaitu sebagai berikut:

- add pada client:

![add](./img/add.png)

- add pada server:

![add_server](./img/add_server.png)

#### 2.d Membuat command see

Setelah client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan see problem. Untuk kodenya yaitu sebagai berikut:

```c#
 if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```

Apabila user memasukkan command see, akan ditampilkan pesan berupa "Problems Available in Online Judge" dan client akan mengirimkan perintah "see" kepada server. Command ini akan membuat server mengembalikan daftar judul dan pembuat problem. Untuk kodenya yaitu sebagai berikut:

```c#
   else if (same(cmd, "see"))
          {
            printf("Problems Available in Online Judge:\n");
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            while (readerConn = read(sock, buffer, 1100))
            {
              if (same(buffer, "e"))
                break;
              printf("%s", buffer);
            }
            continue;
```

Di sisi server, jika server mendapatkan command "see", server akan membuka file problems.tsv dengan fungsi fopen mode "r" untuk membaca problem dan mengirimkan line demi line berisi judul problem dan pengarangnya kepada client sebelum menutup file tersebut dengan fclose. Untuk kodenya yaitu sebagai berikut:

```c#
          else if (same("see", buffer))
          {
            fp = fopen("problems.tsv", "r");
            if (!fp)
            {
              send(serverSocket, "e", sizeof("e"), 0);
              memset(buffer, 0, sizeof(buffer));
              continue;
            }
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            while ((fileReader = getline(&line, &len, fp) != -1))
            {
              problem temp_problem;
              storeProblemTSV(&temp_problem, line);
              char message[1100];
              sprintf(message, "%s by %s", temp_problem.problem_title, temp_problem.author);
              send(serverSocket, message, 1100, 0);
            }
            send(serverSocket, "e", sizeof("e"), 0), fclose(fp);
            memset(buffer, 0, sizeof(buffer));
          }
        }
```

Untuk dokumentasi command see yaitu sebagai berikut:

![see](./img/see.png)

#### 2.e Membuat command download

Setelah client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan download problem. Untuk kodenya yaitu sebagai berikut:

```c#
 if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```

Jika client memasukkan command download [namafile], client akan mengirimkan command tersebut menggunakan fungsi send kepada server. Untuk kodenya yaitu sebagai berikut:

```c#
          else if (same(cmd, "download"))
          {
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            scanf("%s", cmd);
            send(sock, cmd, 1100, 0);
            readerConn = read(sock, buffer, 1100);
            int numLine = atoi(buffer);
            char inputTarget[1100];
            readerConn = read(sock, inputTarget, 1100);
            for (int i = 0; i < numLine; i++)
            {
              FILE *fpClient = fopen(inputTarget, "r");
              fpClient = fopen(inputTarget, "a+");
              char fileContent[1100];
              readerConn = read(sock, fileContent, 1100);
              fprintf(fpClient, "%s\n", fileContent), fclose(fpClient);
            }
            memset(buffer, 0, sizeof(buffer));
            readerConn = read(sock, buffer, 1100);
            char fileReady[] = "File is ready to be downloaded.\n", descTarget[1100];
            if (same(buffer, fileReady))
            {
              memset(buffer, 0, sizeof(buffer));
              readerConn = read(sock, buffer, 1100);
              strcpy(descTarget, buffer);
              printf("Downloading description & input from problem %s..\n", cmd);
              int des_fd = open(descTarget, O_WRONLY | O_CREAT | O_EXCL, 0700);
              if (!des_fd)
                perror("can't open file"), exit(EXIT_FAILURE);
              int file_read_len;
              char buff[1100];
              while (1)
              {
                memset(buff, 0, 1100);
                file_read_len = read(sock, buff, 1100);
                write(des_fd, buff, file_read_len);
                break;
              }
              printf("Successfully downloading the files for problem %s to the desired client directory!\n", cmd);
            }
            memset(buffer, 0, sizeof(buffer));
            continue;
```

Untuk kode pada server yaitu sebagai berikut:

```c#
          else if (same("download", buffer))
          {
            readerConn = read(serverSocket, buffer, 1100);
            bool foundDesc = false, foundInput = false;
            char pathDownload[1100] = "problems/", descPath[1100], descTarget[1100];
            strcat(pathDownload, buffer);
            strcpy(descPath, pathDownload);
            strcat(descPath, "/description.txt");
            char inputPath[1100], inputTarget[1100], outputPath[1100], outputTarget[1100];
            strcpy(inputPath, pathDownload);
            strcat(inputPath, "/input.txt");
            strcpy(outputPath, pathDownload);
            strcat(outputPath, "/output.txt");
            char foundSuccess[] = "File is ready to be downloaded.\n", notFound[] = "No such file found.\n";
            fp = fopen(descPath, "r");
            FILE *source, *target;
            source = fopen(descPath, "r");
            char *line = NULL, *line2 = NULL, *line3 = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            int num = 0, fd, readLength;
            while ((fileReader = getline(&line, &len, source) != -1))
            {
              num++;
              if (num > 3)
              {
                char pathContent[105];
                int i = 0, j = 0;
                while (line[i] != ':')
                  i++;
                line[i] = '\0';
                i += 2;
                while (line[i] != '\n')
                  pathContent[j++] = line[i++];
                pathContent[j] = '\0';
                if (num == 4)
                  strcpy(descTarget, pathContent);
                if (num == 5)
                  strcpy(inputTarget, pathContent);
                if (num == 6)
                  strcpy(outputTarget, pathContent);
              }
            }
            int numLine = 0;
            FILE *fpSource = fopen(inputPath, "r");
            ssize_t len2 = 0;
            ssize_t fileReader2;
            bool checkSubmission = true;
            while ((fileReader2 = getline(&line2, &len2, fpSource) != -1))
              numLine++;
            fclose(fpSource);
            fpSource = fopen(inputPath, "r");
            char buf[1100];
            sprintf(buf, "%d", numLine);
            send(serverSocket, buf, 1100, 0);
            send(serverSocket, inputTarget, 1100, 0);
            ssize_t len3 = 0;
            ssize_t fileReader3;
            while ((fileReader3 = getline(&line3, &len3, fpSource) != -1))
            {
              char outputSource[105];
              int i = 0;
              while (line3[i] != '\n')
                outputSource[i] = line3[i], i++;
              outputSource[i] = '\0';
              send(serverSocket, outputSource, 1100, 0);
            }
            fclose(fpSource);
            if (fp)
              foundDesc = true;
            if (!foundDesc)
              send(serverSocket, notFound, 1100, 0);
            else
            {
              send(serverSocket, foundSuccess, 1100, 0);
              send(serverSocket, descTarget, 1100, 0);
              fd = open(descPath, O_RDONLY);
              if (!fd)
                perror("Fail to open"), exit(EXIT_FAILURE);
              while (1)
              {
                memset(descPath, 0x00, 1100);
                readLength = read(fd, descPath, 1100);
                if (readLength == 0)
                  break;
                else
                  send(serverSocket, descPath, readLength, 0);
              }
              close(fd);
            }
            fclose(fp);
          }
```

Untuk dokumentasi command download yaitu sebagai berikut:

![download](./img/download.png)

#### 2.f Membuat command submit

Jika client memasukkan command submit [judul-problem] [path-file-output], client akan mengirim command tersebut kepada server kemudian memasukkan [judul-problem] dan [path-file-output] untuk dikirimkan juga kepada server. Client kemudian akan mencoba untuk membuka file dan membacanya. Jika file tidak tersedia, akan dikembalikan pesan "Output file is not exist!" dan break akan terjadi. Jika file tersedia, program akan masuk ke dalam loop untuk mengambil line demi line dari output submission menggunakan fungsi getline untuk dikirimkan kepada server. Setelah tidak ada line untuk diambil lagi, program akan membaca perintah yang dikembalikan oleh server dan menampilkan hasil dari submission. Untuk kodenya yaitu sebagai berikut:

```c#
          else if (same(cmd, "submit"))
          {
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            char namaProblem[1100], filePath[1100];
            scanf("%s %s", namaProblem, filePath);
            send(sock, namaProblem, 1100, 0);
            FILE *fpC = fopen(filePath, "r");
            if (!fpC)
            {
              printf("Output file is not exist!\n");
              break;
            }
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            bool checkSubmission = true;
            while ((fileReader = getline(&line, &len, fpC) != -1))
            {
              char outputSubmission[105];
              int i = 0, j = 0;
              while (line[i] != ':')
                i++;
              line[i++] = '\0';
              while (line[i] != '\n')
                outputSubmission[j++] = line[i++];
              outputSubmission[j] = '\0';
              send(sock, outputSubmission, 105, 0);
            }
            memset(buffer, 0, sizeof(buffer));
            readerConn = read(sock, buffer, 1100);
            printf("Your output.txt file for problem %s is submitted!\nResult: %s\n", namaProblem, buffer);
          }
```

Pada sisi server, server akan membaca informasi [judul-problem] [path-file-output] dari soal dan membuka file yang berada di destinasi path yang diberikan. Server kemudian mengambil line demi line menggunakan loop untuk membandingkan antara isi dari output yang disubmit oleh client dan output yang tersedia di database. Jika yang disubmit tidak sama dengan yang ada di database server, akan dikembalikan false kepada client. Untuk kodenya yaitu sebagai berikut:

```c#
          else if (same("submit", buffer))
          {
            char problemName[1100], pathSource[1100] = "problems/";
            readerConn = read(serverSocket, problemName, 1100);
            strcat(pathSource, problemName);
            strcat(pathSource, "/output.txt");
            FILE *fpSource = fopen(pathSource, "r");
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            bool checkSubmission = true;
            while (fileReader = getline(&line, &len, fpSource) != -1)
            {
              char outputSource[105], outputSubmission[105];
              int i = 0, j = 0;
              while (line[i] != ':')
                i++;
              line[i++] = '\0';
              while (line[i] != '\n')
                outputSource[j++] = line[i++];
              outputSource[j] = '\0';
              readerConn = read(serverSocket, outputSubmission, 105);
              int source = atoi(outputSource), submit = atoi(outputSubmission);
              printf("[%s] source: %d dan submission: %d\n", submit == source ? "AC" : "WA", source, submit);
              if (submit != source)
                checkSubmission = false;
            }
            printf("%s is %s on problem %s!\n", user.name, checkSubmission ? "accepted (AC)" : "having a wrong answer (WA)", problemName);
            send(serverSocket, checkSubmission ? "AC\nAccepted in Every Testcases\n" : "WA\nWA in the First Testcase\n", 1100, 0);
          }
```

#### 2.g Membuat command multiple connection

Program untuk melakukan multiple connection dengan memeriksa berapa banyak client yang terkoneksi. Apabila terdapat tepat satu client, server akan mengirimkan pesan connected kepada client, sedangkan jika tidak maka server akan mengirimkan pesan failed. Selama terbaca bahwa total client lebih dari satu, server akan mengecek banyak client yang terhubung. Jika setelah itu jumlah client adalah satu, maka server akan mengirimkan pesan connected. Namun, jika tidak, server akan mengirimkan pesan failed. Failed akan menampilkan pada client yang gagal terhubung bahwa ada client yang sedang terkoneksi dan untuk menunggu koneksi untuk diakhiri. Untuk kodenya yaitu sebagai berikut:

```c#
  char buffer[1100] = {0}, connected[1100] = "Connected", failed[1100] = "Other people are connected, please wait for them";
  int readerConn, serverSocket = *(int *)tmp;
  if (total == 1)
    send(serverSocket, connected, 1100, 0);
  else
    send(serverSocket, failed, 1100, 0);
  while (total > 1)
  {
    readerConn = read(serverSocket, buffer, 1100);
    if (total == 1)
      send(serverSocket, connected, 1100, 0);
    else
      send(serverSocket, failed, 1100, 0);
  }
```

### Kendala
Kelompok kami kesulitan dalam memahami logic dari soal dan kurangnya sumber referensi untuk belajar

---
## Soal 3
Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.

<ol type="a">
    <li>Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif</li>
    <li>Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.</li>
    <li>Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.</li>
    <li>Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.</li>
    <li>Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command</li>
</ol>

Catatan:
- Kategori folder tidak dibuat secara manual, harus melalui program C
- Program ini tidak case sensitive. Contoh: JPG dan jpg adalah sama
- Jika ekstensi lebih dari satu (contoh “.tar.gz”) maka akan masuk ke folder dengan titik terdepan (contoh “tar.gz”)
- Dilarang juga menggunakan fork, exec dan system(), kecuali untuk bagian zip pada soal d


### Penyelesaian

Kode:<br>
[Source code](./soal3/)

#### Soal 3.a Mengekstrak zip
Langkah pertama yaitu mengekstrak file hartakarun.zip yang telah disediakan di soal. Lalu menggunakan fungsi popen() dengan menggunakan argumen pertama yaitu value, dimana variabel value berisi path ke file .zip-nya dan argumen kedua yaitu r untuk membaca filenya. Lalu selanjutnya menggunakan fungsi fread pada variabel chars_read akan memprint proses ekstraksi file di dalam arsip ke terminal. Fungsi yang bertanggung jawab dalam hal ini adalah fungsi extract().

```c#
void extract(char *value) 
{
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer,'\0',sizeof(buffer));

    file = popen(value ,"r");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n",buffer);
        pclose(file);
    }
}
```

#### Soal 3.b Mengsortir file dalam folder

Lalu selanjutnya untuk mengkategorikan file, program akan memanggil fungsi sortFile() untuk menjalankan tugas tersebut. Berikut tampilan fungsi sortFile():

```c#
void sortFile(char *from)
{
    struct_path s_path;
    struct dirent *dp;
    DIR *dir;

    flag = 1;
    int index = 0;
    strcpy(s_path.cwd, "/home/aga/shift3/hartakarun");

    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move, (void *)&s_path);
            sleep(1);
            index++;
        }
    }

    if(!flag){
        printf("Maaf kawan, lupakan saja :)\n");
    }else {
        printf("Kamu bisa lanjut, tapi jangan berharap lebih :)\n");
    }
}
```

Di fungsi ini program akan membaca directory hasil ekstrak file .zip dan mengecek satu-persatu filenya. Informasi mengenai directory asal dan nama filenya disimpan ke dalam variabel fileName. Tugas ini dilakukan pada potongan code di bawah ini:

```c#
    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);
```

#### Soal 3.c Mengsortir file menggunakan thread
Pembuatan thread dilakukan pada fungsi sortFile() yang juga memanggil fungsi move().

```c#
pthread_create(&tid[index], NULL, move, (void *)&s_path);
```

Pada fungsi move() program akan meamnggil fungsi moveFile() kemudian keluar dari thread.

```c#
{
    struct_path s_paths = *(struct_path*) s_path;
    moveFile(s_paths.from, s_paths.cwd);
    pthread_exit(0);
}
```

Berikut merupakan tampilan fungsi moveFile():

```c#
void moveFile(char *p_from, char *p_cwd)
{
    char file_name[300], file_ext[30], temp[300];
    char *buffer;

    strcpy(temp, p_from);
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }

    toLower(file_ext);

    // new dest
    strcat(p_cwd, "/");
    strcat(p_cwd, file_ext);
    strcat(p_cwd, "/");
    strcat(p_cwd, file_name);

    // information
    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    // create dir and move file
    createDir(file_ext);
    rename(p_from, p_cwd);
}
```

Langkah awal pada fungsi ini yaitu nama file ke dalam variabel file_name dengan bantuan variabel buffer yang sebelumnya akan diisi nama file dengan bantuan fungsi strtok untuk mencari delimiter "/".

```c#
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }
```

Selanjutnya menentukan ekstensi file dengan bantuan variabel buffer yang akan berisi format file setelah diinisiasi hasil dari strtok dengan delimiter ".". Terdapat pengecualian pada pengkategorian file ini yaitu apabila variabel buffer berisi NULL yang artinya ekstensi file akan dikategorikan hidden (apabila file diawali karakter ".") dan unknown (apabila tidak memenuhi kedua kondisi).

```c#
strcpy(temp, file_name);
    
buffer = strtok(temp, ".");
buffer = strtok(NULL, "");

if(file_name[0] == '.'){
    strcpy(file_ext, "Hidden");
}else if(buffer != NULL){
    strcpy(file_ext, buffer);
}else{
    strcpy(file_ext, "Unknown");
}
```

Selanjutnya adalah membuat destinasi dari file dengan menambahkan ekstensi dan nama file ke dalam path directory lama menggunakan strcat().

```c#
strcat(p_cwd, "/");
strcat(p_cwd, file_ext);
strcat(p_cwd, "/");
strcat(p_cwd, file_name);
```

Kemudian langkah terakhir adalh menampilkan informasi file dan membuat directory berdasarkan ekstensi filenya.

```c#
// information
printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

// create dir and move file
createDir(file_ext);
rename(p_from, p_cwd);
```

#### Soal 3.d Menjalankan client-server
Belum dituntaskan

#### Soal 3.e Mengirim file
Belum dituntaskan

Output:
- Extract zip

![extract zip](./img/unzip.png)

- Informasi file

![informasi file](./img/informasi-file.png)

- Pengkategorian file berdasar ekstensi

![categorized files](./img/ekstensi-file.png)

- File yang *hidden*

![file hidden](./img/hidden.png)

- File yang *unknown*

![file unknown](./img/unknown.png)

### Kendala
Kelompok kami kesulitan dalam melakukan unzip file tanpa menggunakan execv.

