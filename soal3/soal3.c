#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <pthread.h>
#include <limits.h>

pthread_t tid[200];
int flag = 1;

typedef struct struct_path{
    char from[PATH_MAX];
    char cwd[PATH_MAX];
} struct_path;

void createDir(void *param);
void toLower(char *param);
int checkFile(const char *path);
void moveFile(char *p_from, char *p_cwd);
void *move(void *s_path);
void sortFile(char *from);
void extract(char *file);

int main() {
    struct_path s_path;
    getcwd(s_path.cwd, sizeof(s_path.cwd));

    strcpy(s_path.from, "/home/rasy/soal-shift-sisop-modul-3-ita01-2022/soal3/hartakarun");
    extract("unzip /home/rasy/soal-shift-sisop-modul-3-ita01-2022/soal3/hartakarun.zip");
    sortFile(s_path.from);

    return 0;
}

// move file
void moveFile(char *p_from, char *p_cwd)
{
    char file_name[300], file_ext[30], temp[300];
    char *buffer;

    strcpy(temp, p_from);
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }

    toLower(file_ext);

    // new dest
    strcat(p_cwd, "/");
    strcat(p_cwd, file_ext);
    strcat(p_cwd, "/");
    strcat(p_cwd, file_name);

    // information
    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    // create dir and move file
    createDir(file_ext);
    rename(p_from, p_cwd);
}

// move
void *move(void *s_path)
{
    struct_path s_paths = *(struct_path*) s_path;
    moveFile(s_paths.from, s_paths.cwd);
    pthread_exit(0);
}

// sorting file by category
void sortFile(char *from)
{
    struct_path s_path;
    struct dirent *dp;
    DIR *dir;

    flag = 1;
    int index = 0;
    strcpy(s_path.cwd, "/home/rasy/soal-shift-sisop-modul-3-ita01-2022/soal3/hartakarun");

    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move, (void *)&s_path);
            sleep(1);
            index++;
        }
    }

    if(!flag){
        printf("Maaf kawan, lupakan saja :)\n");
    }else {
        printf("Kamu bisa lanjut, tapi jangan berharap lebih :)\n");
    }
}

// convert upper to lower
void toLower(char *param)
{
    int i = 0;
    for (i; param[i]; i++){
        param[i] = tolower(param[i]);
    }
}

// create dir based on param
void createDir(void *param)
{
    char *value = (char *) param;
    char path[PATH_MAX];

    strcpy(path, "/home/rasy/soal-shift-sisop-modul-3-ita01-2022/soal3/hartakarun/");
    strcat(path, value);

    // printf("create dir --> %s", path);

    int res = mkdir(path, 0777);
    if(res == -1){}
}

// check is file or folder
int checkFile(const char *path)
{
    struct stat stat_path;
    stat(path, &stat_path);
    return S_ISREG(stat_path.st_mode);
}

// extract zip file
void extract(char *value) 
{
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer,'\0',sizeof(buffer));

    file = popen(value ,"r");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n",buffer);
        pclose(file);
    }
}